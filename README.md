```
                                                      ██           
                                          ██    ██████             
                                          ██  ██▒▒▒▒██             
                                        ██████▒▒▒▒▒▒██             
                                        ██  ██▒▒▒▒██               
                                      ██    ██████                 
                                    ██    ██                       
                                    ██    ██                       
                                  ██        ██                     
                                ██            ████████             
                            ████████        ██▒▒▒▒▒▒▒▒██           
                          ██▒▒▒▒▒▒▒▒██    ██▒▒▒▒▒▒▒▒▒▒▒▒██         
                        ██▒▒▒▒▒▒  ▒▒▒▒██  ██▒▒▒▒▒▒▒▒▒▒▒▒██         
                        ██▒▒▒▒▒▒▒▒  ▒▒██  ██▒▒  ▒▒▒▒▒▒▒▒██         
                        ██▒▒▒▒▒▒▒▒  ▒▒██  ██▒▒▒▒    ▒▒▒▒██         
                        ██▒▒▒▒▒▒▒▒▒▒▒▒██    ██▒▒▒▒▒▒▒▒██           
                          ██▒▒▒▒▒▒▒▒██        ████████             
                            ████████                               
```	


# CherryPick
The idea behind this is to provide access to the fbr files stored across hosts and allow the user to upload them to s3 and download them to their local machine without needing to login to the related machines. Currently it is doing this by sshing into the selected machine, getting a list of the files in `C:/Flashbacks` and then cross referencing them with the files in the `flashback01` s3 bucket. The resultant list is displayed to the user in a table where they can either upload files from the machine into s3 or download the file from s3 onto their own machine for vieweing. It uses sftp to transfer the files and the boto3 to store them in s3. This is somewhat slow and depends a lot on the connection of the particular vessel at the particular time.

