from api import get_bucket_contents, list_files, get_all_files, put_file_s3, get_download_link
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/list-local")
def list_local():
    host = request.args.get('host')
    print("getting local files for: " + host)
    return jsonify(results=list_files(host))


@app.route("/list-cloud")
def list_cloud():
    host = request.args.get('host')
    print("getting cloud files for: " + host)
    return jsonify(results=get_bucket_contents(host))


@app.route("/put-s3")
def put_s3():
    host = request.args.get('host')
    video = request.args.get('video')
    print("putting " + video + " on " + host)
    return jsonify(put_file_s3(host, video))


@app.route("/download")
def download():
    video = request.args.get('video')
    print(video)
    print("downloading..")
    return jsonify(get_download_link(video))


@app.route("/list-files")
def files():
    host = request.args.get('host')
    print("getting files for: " + host)
    print("|=========================|")
    items = get_all_files(host)
    for item in items:
        print("\t|: " + str(item))
    print("|=========================|")
    return jsonify(results=items)
