#!/bin/python3
import os
import subprocess
import logging
import boto3
from botocore.exceptions import ClientError


# scp_directory = "/Users/Engineer/Documents/FlashBack Movies"
# dir_command = ' \'dir /b /a-d C:\\Users\\Engineer\\Documents\\"FlashBack Movies"\\\''
scp_directory = "\\C:\\Flashbacks\\"
sftp_directory = "/C:/Flashbacks/"
dir_command = ' \'dir /b /a-d C:\\Flashbacks\\\''

# Vessels
grace_backup = {'name': 'grace-backup', 'ip': '10.2.94.131', 'cert': 'gracebackupbdu', 'stored_dir': 'C:\Flashbacks'}
finlandia_backup = {'name': 'finlandia-backup', 'ip': '10.2.73.90', 'cert': 'finlandiabackupbdu', 'stored_dir': 'C:\Flashbacks'}
finlandia = {'name': 'finlandia', 'ip': '10.2.73.131', 'cert': 'finlandiabdu', 'stored_dir': 'C:\Flashbacks'}
megastar = {'name': 'megastar', 'ip': '10.2.71.110', 'cert': 'megastarbdu', 'stored_dir': 'C:\Flashbacks'}
hosts = [grace_backup, finlandia_backup, finlandia, megastar]


# def format_upload_name(hostname, file_name):
#     name = file_name.replace(" ", "")
#     name = name.replace("Untitled", "")
#     return hostname + '-' + name


def put_file_s3(hostname, file_name):
    get_file(gethost(hostname), file_name)
    print("put_file_s3")
    print(hostname)
    print(file_name)
    print("-----------")
    bucket = "flashback01"
    object_name = hostname + "/" + file_name
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file("/tmp/" + file_name.replace(" ", "-"), bucket, object_name)
        print(response)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def connection_string(host):
    return "sshpass -p c@vot3c ssh Engineer@" + host['ip']


def scp_string(host, filename):
    return "sshpass -p c@vot3c scp -C -l 200000 -T " + "Engineer@" + host['ip'] + ":" + scp_directory + "/" + filename + " /tmp/" + filename.replace(" ", "-")


def sftp_string(host, filename):
    return "sshpass -p c@vot3c sftp  -R 128 -B 65536 Engineer@" + host['ip'] + ":" + sftp_directory + filename + " /tmp/" + filename.replace(" ", "-")


def gethost(name):
    return next(item for item in hosts if item["name"] == name)


def list_files(hostname):
    host = gethost(hostname)
    connection = connection_string(host)
    try:
        x = subprocess.check_output([connection + dir_command], shell=True, text=True)
        video_names = x.splitlines()
        return video_names
    except Exception as ex:
        print(ex)
        return []


def get_bucket_contents(hostname):
    conn = boto3.client('s3')
    items = conn.list_objects(Bucket='flashback01')['Contents']
    return items


def get_file(host, filename):
    command = sftp_string(host, filename)
    x = subprocess.check_output([command], shell=True, text=True)
    print(x)


def get_download_link(filename):
    s3_client = boto3.client('s3')
    try:
        response = s3_client.generate_presigned_url('get_object', Params={'Bucket': "flashback01", 'Key': filename}, ExpiresIn=3600)
    except Exception as e:
        logging.error(e)
        return None
    return response


def get_all_files(hostname):
    files = []
    cloudfiles = get_bucket_contents(hostname)
    localfiles = list_files(hostname)
    #local_formatted = list(map(lambda x: format_upload_name(hostname, x), localfiles))
    print("#----------------#")
    for item in cloudfiles:
        if item['Key'].startswith(hostname):
            print(item)
            local = False

            for lf in localfiles:
                if lf in item['Key']:
                    local = True
                    for f in localfiles:
                        if f == lf:
                            print("removing.. " + f)
                            localfiles.remove(f)

            tags = {"S3": True, "local": local}
            files.append({
                'name': item['Key'],
                'date': item['LastModified'],
                'size': item['Size'],
                'tags': tags
            })
    for item in localfiles:
        files.append({'name': item, 'tags': {'S3': False, 'local': True}})
    print("#----------------#")
    return files

# To be done.
# def send_file(host)
# def list_s3_files(host)
