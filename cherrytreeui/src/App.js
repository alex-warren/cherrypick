import './App.css';
import logo from './cherry.jpg';
import Tabli from './Table.js';
import React from 'react';

function App() {

  return(
    <div className="App">
      <header className="App-header">
          <img src={logo} className="App-logo" alt="cherry" />
	  </header>
	<Tabli/>
    </div>
  );
}

export default App;
