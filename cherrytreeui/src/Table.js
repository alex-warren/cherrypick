import React, { useState } from 'react';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Table, Space } from 'antd';

const { Option } = Select;


const download = (host, video) => {
    console.log(host);
    fetch('http://127.0.0.1:5000/download?video=' + host["name"])
	.then(function (response) {
	    return response.json();
	}).then(function (result) {
	    window.open(result);
	});
}


export default function Tabli() {
    const [files, setFiles] = useState([]);
    const [loading, setLoading] = useState(false);
    const [host, setHost] = useState();

    const upload = (host, video) => {
	console.log("uploading... " + video["name"] + " to " + host);
	alert("Uploading is slow at the moment, bear with me..")
	setLoading(true);
	video["loading"] = true;
	fetch('http://127.0.0.1:5000/put-s3?host=' + host + '&video=' + video["name"])
	    .then(function (response) {
		console.log(response);
		setLoading(false);
		video["loading"] = false;
	    });
    }

    function formatSize(size) {
	if (size ) {
	    var mb = (size / 1000000).toFixed(2);
	    return mb.toString() + "mb";
	}
	return "-"
    }

    const columns = [
	{
	    title: 'name',
	    dataIndex: 'name',
	    key: 'name',
	    render: text => <a>{text}</a>,
	},
	{
	    title: 'date',
	    dataIndex: 'date',
	    key: 'date',
	},
	{
	    title: 'size',
	    dataIndex: 'size',
	    key: 'size',
	    render: size => <p>{formatSize(size)}</p>,
	},
	{
	    title: 'S3',
	    key: 'S3',
	    dataIndex: 'tags',
	    render: tags => <p>{tags["S3"] ? "True" : "False"}</p>,
	},
	{
	    title: 'local',
	    key: 'local',
	    dataIndex: 'tags',
	    render: tags => <p>{tags["local"] ? "True" : "False"}</p>,
	},
	{
	    title: 'Action',
	    key: 'action',
	    render: (text, record) => {
		console.log(text, record)
		if (!text["tags"]["S3"]) {
		    if(text["loading"] === true) {
			return (
			    <Space size="middle">
				<p>Uploading...</p>
			    </Space>
			);
		    } else {
			return (
			    <Space size="middle">
				<a onClick={e => upload(host, text)}>Upload</a>
			    </Space>
			);
		    }
		}
		return (
		    <Space size="middle">
			<a onClick={e => download(text)} >Download</a>
		    </Space>
		);
	    },
	},
    ];

    function changeVessel(e) {
	setHost(e);
	setFiles([]);
	setLoading(true);
	fetch('http://127.0.0.1:5000/list-files?host=' + e)
	    .then(function (response) {
		return response.json();
	    }).then(function (result) {
		var x = result["results"];
		setFiles(x);
		setLoading(false)
	    });
    }

    return (
	<>
	  <Select style={{ width: 240 }} onChange={e => changeVessel(e)} disabled={loading} >
	    <Option value="grace-backup">Grace Backup</Option>
	    <Option value="finlandia-backup">Finlandia Backup</Option>
	    <Option value="megastar">Megastar</Option>
	    <Option value="finlandia">Finlandia</Option>
	    <Option value="grace" disabled>Grace</Option>
	  </Select>
	  <Table columns={columns} loading={loading} dataSource={files} />
	</>
    );
}
